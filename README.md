# Código Libro _(codigolibro)_

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> A telegram bot used to track the holders of books in a communal library.

## Install

TODO

## Usage

TODO

## Maintainer

[elopio](https://keybase.io/elopio).

## License

[GNU GPL v3](https://gitlab.com/jaquerespeis/codigolibro/blob/master/LICENSE).
