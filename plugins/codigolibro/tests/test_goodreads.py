import unittest
import unittest.mock

import libraries.goodreads


class TestGoodreads(unittest.TestCase):

    def test_init(self):
        goodreads = libraries.goodreads.Goodreads('test_developer_key')
        self.assertEqual(goodreads._developer_key, 'test_developer_key')

    def test_search(self):
        goodreads = libraries.goodreads.Goodreads('dummy')
        with unittest.mock.patch(
                'libraries.goodreads.goodreads_api_client.Client.search_book'
        ) as mock_search_book:
            mock_search_book.return_value = {
                'results': {
                    'work': [
                        {'best_book': {'id': {'#text': '1'}}},
                        {'best_book': {'id': {'#text': '2'}}},
                        {'best_book': {'id': {'#text': '3'}}},
                        {'best_book': {'id': {'#text': '4'}}},
                    ]
                }
            }
            number_of_results = 3
            results = goodreads.search_book(
                'test book', number_of_results)
        mock_search_book.assert_called_with('test book')
        self.assertEqual(results, [1, 2, 3])

    def test_get_book_id(self):
        goodreads = libraries.goodreads.Goodreads('dummy')
        with unittest.mock.patch(
               'libraries.goodreads.goodreads_api_client.Client'
        ) as mock_client:
            mock_client.return_value.Book.show.return_value = {
                'link': 'test link'
            }
            link = goodreads.get_book_link('test id')
        mock_client.return_value.Book.show.assert_called_with('test id')
        self.assertEqual(link, 'test link')

    def test_get_id_from_url(self):
        # TODO: use testscenarios
        # --elopio - 20191202
        self.assertEqual(
            libraries.goodreads.Goodreads.get_id_from_url(
                'https://www.goodreads.com/book/show/123.test_book'),
            123)
        self.assertEqual(
            libraries.goodreads.Goodreads.get_id_from_url(
                'https://www.goodreads.com/book/show/123-test_book'),
            123)
