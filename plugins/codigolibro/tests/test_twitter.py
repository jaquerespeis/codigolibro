import unittest
import unittest.mock

import libraries.twitter


class TestTwitter(unittest.TestCase):

    def test_init(self):
        twitter = libraries.twitter.Twitter(
            'test_consumer_key',
            'test_consumer_secret',
            'test_access_token_key',
            'test_access_token_secret'
        )
        self.assertEqual(twitter._consumer_key, 'test_consumer_key')
        self.assertEqual(twitter._consumer_secret, 'test_consumer_secret')
        self.assertEqual(twitter._access_token_key, 'test_access_token_key')
        self.assertEqual(
            twitter._access_token_secret, 'test_access_token_secret')

    def test_send_tweet(self):
        twitter = libraries.twitter.Twitter('dummy', 'dummy', 'dummy', 'dummy')
        with unittest.mock.patch(
                'libraries.twitter.twitter.Api.PostUpdate'
        ) as mock_post_update:
            twitter.send_tweet('test message')
        mock_post_update.assert_called_with('test message')
