import unittest
import unittest.mock

import libraries.mongodb


class TestMongoDB(unittest.TestCase):

    def test_init(self):
        mongodb = libraries.mongodb.MongoDB(
            'test_user', 'test_password', 'test_cluster')
        self.assertEqual(mongodb._user, 'test_user')
        self.assertEqual(mongodb._password, 'test_password')
        self.assertEqual(mongodb._cluster, 'test_cluster')

    def test_replace_one(self):
        mongodb = libraries.mongodb.MongoDB('dummy', 'dummy', 'dummy')
        with unittest.mock.patch(
                'libraries.mongodb.pymongo.MongoClient'
        ) as mock_client:
            mock_collection = unittest.mock.Mock()
            mock_client.return_value = {
                'test database': {'test collection': mock_collection}
            }
            mongodb.replace_one(
                'test database',
                'test collection',
                'test filter',
                'test replacement',
                'test upsert'
            )
        mock_collection.replace_one.assert_called_with(
            filter='test filter',
            replacement='test replacement',
            upsert='test upsert'
        )

    def test_find_one(self):
        mongodb = libraries.mongodb.MongoDB('dummy', 'dummy', 'dummy')
        with unittest.mock.patch(
                'libraries.mongodb.pymongo.MongoClient'
        ) as mock_client:
            mock_collection = unittest.mock.Mock()
            mock_collection.find_one.return_value = 'test result'
            mock_client.return_value = {
                'test database': {'test collection': mock_collection}
            }
            result = mongodb.find_one(
                'test database',
                'test collection',
                'test filter',
            )
        mock_collection.find_one.assert_called_with(filter='test filter')
        self.assertEqual(result, 'test result')
