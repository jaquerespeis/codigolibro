import twitter


class Twitter():

    def __init__(
            self,
            consumer_key, consumer_secret,
            access_token_key, access_token_secret):
        self._consumer_key = consumer_key
        self._consumer_secret = consumer_secret
        self._access_token_key = access_token_key
        self._access_token_secret = access_token_secret

    def _get_api_client(self):
        if not hasattr(self, '_api_client'):
            self._api_client = twitter.Api(
                self._consumer_key,
                self._consumer_secret,
                self._access_token_key,
                self._access_token_secret)
        return self._api_client

    def send_tweet(self, text):
        return self._get_api_client().PostUpdate(text)
