import goodreads_api_client


class Goodreads():

    def __init__(self, developer_key):
        self._developer_key = developer_key

    def _get_api_client(self):
        if not hasattr(self, '_api_client'):
            self._api_client = goodreads_api_client.Client(
                self._developer_key)
        return self._api_client

    def search_book(self, query, number_of_results):
        results = self._get_api_client().search_book(query)['results']
        ids = []
        for result in results['work'][:number_of_results]:
            id_ = int(result['best_book']['id']['#text'])
            ids.append(id_)
        return ids

    def get_book_link(self, book_id):
        return self._get_api_client().Book.show(book_id)['link']

    def get_id_from_url(goodreads_url):
        return int(
            goodreads_url.replace(
                'https://www.goodreads.com/book/show/', ''
            ).split('-')[0].split('.')[0])
