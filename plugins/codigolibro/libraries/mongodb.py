import pymongo


class MongoDB():

    def __init__(self, user, password, cluster):
        self._user = user
        self._password = password
        self._cluster = cluster

    def _get_api_client(self):
        if not hasattr(self, '_api_client'):
            self._api_client = pymongo.MongoClient(
                'mongodb+srv://{}:{}@{}-xgrnm.mongodb.net/'
                'test?retryWrites=true&w=majority'.format(
                    self._user,
                    self._password,
                    self._cluster))
        return self._api_client

    def replace_one(
            self, database, collection, filter_, replacement, upsert):
        collection = self._get_api_client()[database][collection]
        collection.replace_one(
            filter=filter_,
            replacement=replacement,
            upsert=upsert)

    def find_one(self, database, collection, filter_):
        collection = self._get_api_client()[database][collection]
        return collection.find_one(filter=filter_)
