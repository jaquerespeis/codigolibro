import errbot
import twitter

import libraries.goodreads
import libraries.mongodb
import libraries.twitter


class CodigoLibro(errbot.BotPlugin):
    """
    Share your books.
    """

    def _get_goodreads(self):
        if not hasattr(self, '_goodreads'):
            self._goodreads = libraries.goodreads.Goodreads(
                developer_key=self.bot_config.GOODREADS_DEVELOPER_KEY)
        return self._goodreads

    def _get_twitter(self):
        if not hasattr(self, '_twitter'):
            self._twitter = libraries.twitter.Twitter(
                consumer_key=self.bot_config.TWITTER_CONSUMER_KEY,
                consumer_secret=self.bot_config.TWITTER_CONSUMER_SECRET,
                access_token_key=self.bot_config.TWITTER_ACCESS_TOKEN_KEY,
                access_token_secret=self.bot_config.TWITTER_ACCESS_TOKEN_SECRET)
        return self._twitter

    def _get_mongodb(self):
        if not hasattr(self, '_mongodb'):
            self._mongodb = libraries.mongodb.MongoDB(
                user=self.bot_config.MONGODB_USER,
                password=self.bot_config.MONGODB_PASSWORD,
                cluster=self.bot_config.MONGODB_CLUSTER)
        return self._mongodb

    @errbot.botcmd
    def buscar(self, message, query):
        number_of_results = 3
        results = self._get_goodreads().search_book(query, number_of_results)
        for id_ in results:
            yield self._get_goodreads().get_book_link(id_)

    @errbot.arg_botcmd('goodreads_url', type=str)
    def tengo(self, message, goodreads_url):
        goodreads_id = libraries.goodreads.Goodreads.get_id_from_url(
            goodreads_url)
        self._get_mongodb().replace_one(
            database=self.bot_config.MONGODB_DATABASE,
            collection=self.bot_config.MONGODB_COLLECTION,
            filter_={
                '_id': goodreads_id
            },
            replacement={
                '_id': goodreads_id,
                'user_id': message.frm.id,
                'username': message.frm.username or message.frm.first_name
            },
            upsert=True)
        yield 'Pura vida, el libro ya quedó registrado.'
        yield (
            'Voy a enviarlo a twitter para ver si alguien lo quiere, '
            'y que más gente se nos una.')
        try:
            tweet_status = self._get_twitter().send_tweet(
                'Tenemos un nuevo libro en nuestra biblioteca comunal: ' +
                goodreads_url +
                '\n'
                'Únasenos si quiere leerlo.')
            yield 'https://twitter.com/CodigoLibroCR/status/' + str(tweet_status.id)
        except twitter.error.TwitterError as e:
            print(e)
        yield '¡Dénle retweet!'


    @errbot.arg_botcmd('goodreads_url', type=str)
    def quiero(self, message, goodreads_url):
        goodreads_id = libraries.goodreads.Goodreads.get_id_from_url(
            goodreads_url)
        self._get_mongodb().find_one(
            database=self.bot_config.MONGODB_DATABASE,
            collection=self.bot_config.MONGODB_COLLECTION,
            filter_={'_id': goodreads_id})
        if result:
            return (
                '<a href="tg://user?id={}">{}</a>: '.format(
                    message.frm.id,
                    message.frm.username or message.frm.first_name) +
                'El libro lo tiene <a href="tg://user?id={}">{}</a>.'.format(
                    result['user_id'],
                    result['username']) +
                '\nPónganse de acuerdo para encontrarse y compartirlo.')
        else:
            yield (
                '<a href="tg://user?id={}">{}</a>: '.format(
                    message.frm.id,
                    message.frm.username or message.frm.first_name) +
                'Lo siento, el libro aún no está en nuestra biblioteca.\n'
                'Voy a enviarlo a twitter para ver si alguien lo tiene, '
                'y quiere compartirlo.')
            try:
                tweet_status = self._get_twitter().send_tweet(
                    'Alguien en nuestra biblioteca comunal quiere leer ' +
                    goodreads_url +
                    '\n'
                    'Únasenos si lo tiene y quiere compartirlo.')
                yield 'https://twitter.com/CodigoLibroCR/status/' + str(tweet_status.id)
            except twitter.error.TwitterError as e:
                print(e)
            yield '¡Dénle retweet!'
