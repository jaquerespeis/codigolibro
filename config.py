import os
import logging

# This is a minimal configuration to get you started with the Text mode.
# If you want to connect Errbot to chat services, checkout
# the options in the more complete config-template.py from here:
# https://raw.githubusercontent.com/errbotio/errbot/master/errbot/config-template.py

BACKEND = 'TelegramPatched'

BOT_DATA_DIR = r'/home/ubuntu/workspace/jaquerespeis/codigolibro/data'
BOT_EXTRA_PLUGIN_DIR = r'/home/ubuntu/workspace/jaquerespeis/codigolibro/plugins'
BOT_EXTRA_BACKEND_DIR = r'/home/ubuntu/workspace/jaquerespeis/codigolibro/backends'
BOT_LOG_FILE = r'/home/ubuntu/workspace/jaquerespeis/codigolibro/errbot.log'
BOT_LOG_LEVEL = logging.DEBUG

TELEGRAM_ID_ELOPIO = '43624396'
BOT_ADMINS = (TELEGRAM_ID_ELOPIO, )

BOT_IDENTITY = {
    'token': os.environ.get('CODIGO_LIBRO_TELEGRAM_TOKEN'),
}

CHATROOM_PRESENCE = ()
BOT_PREFIX = '/'
CORE_PLUGINS = ('Help')

TWITTER_CONSUMER_KEY = os.environ.get('TWITTER_CONSUMER_KEY')
TWITTER_CONSUMER_SECRET = os.environ.get('TWITTER_CONSUMER_SECRET')
TWITTER_ACCESS_TOKEN_KEY = os.environ.get('TWITTER_ACCESS_TOKEN_KEY')
TWITTER_ACCESS_TOKEN_SECRET = os.environ.get('TWITTER_ACCESS_TOKEN_SECRET')

GOODREADS_DEVELOPER_KEY = os.environ.get('GOODREADS_DEVELOPER_KEY')

MONGODB_USER = 'codigolibro'
MONGODB_PASSWORD = os.environ.get('MONGODB_PASSWORD')
MONGODB_CLUSTER = 'codigolibro'
MONGODB_DATABASE = 'codigolibro-cr'
MONGODB_COLLECTION = 'codigolibro-cr'
